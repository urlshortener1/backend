FROM python:3.11
LABEL maintainer="Andres De Innocentiis"

ENV PYTHONUNBUFFERED=1
ENV PATH="/scripts:$PATH"

RUN pip install --upgrade pip

COPY ./requirements.txt /requirements.txt

RUN apt-get update && apt-get install -y \
    postgresql-client \
    libjpeg-dev

# Install build dependencies and clean up
RUN apt-get install -y \
    build-essential \
    python3-dev \
    libpq-dev \
    zlib1g-dev \
    libffi-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install Pylint
RUN pip install pylint==3.0 pylint-exit

# Install all dependencies
RUN pip install -r /requirements.txt

# RUN mkdir /us_backend
RUN mkdir -p /us_backend
WORKDIR /us_backend
COPY . .

# COPY ./entrypoint.sh .
RUN chmod +x ./entrypoint.sh

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static
RUN mkdir -p /us_backend/staticfiles

RUN useradd -M user && chown -R user:user /us_backend
USER user

# This is what's gonna run after the image is created UNLESS there's a command overriding it in the docker-compose
CMD ["./entrypoint.sh"]