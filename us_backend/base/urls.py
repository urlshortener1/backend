from django.urls import path
from .views import *

urlpatterns = [    
    path('users/register/', RegisterUserView.as_view(), name='register-user'),
    path('users/login/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('users/logout/', LogoutView.as_view(), name='auth_logout'),
    path('users/delete/', DeleteUserView.as_view(), name='delete-user'),
]
