from django.contrib.auth import get_user_model, logout
from django.contrib.auth.hashers import make_password
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import DestroyModelMixin

from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.tokens import RefreshToken

from .serializers import UserSerializerWithToken, MyTokenObtainPairSerializer, UserSerializer

# Create your views here.


User = get_user_model()



# Login view (with JWT Token authentication)
class MyTokenObtainPairView(TokenObtainPairView):
    """
    A view to obtain JWT tokens for user authentication.

    Attributes:
        serializer_class (Serializer): The serializer class for token obtainment.
    """
    serializer_class = MyTokenObtainPairSerializer


class LogoutView(APIView):
    """
    A view to handle user logout by blacklisting the refresh token.

    Attributes:
        permission_classes (list): The list of permission classes applied to the view.
    """
    permission_classes = [IsAuthenticated]

    def post(self, request):

        try:
            refresh_token = request.data.get("refresh")  # Use .get() to avoid KeyError
            if refresh_token:
                token = RefreshToken(refresh_token)
                token.blacklist()
            logout(request)
            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:

            return Response(status=status.HTTP_400_BAD_REQUEST)
        
        

# Register user:    
class RegisterUserView(APIView):
    """
    A view to register a new user.

    Attributes:
        serializer_class (Serializer): The serializer class for user registration.
    """
    serializer_class = UserSerializerWithToken
    
    def post(self, request, *args, **kwargs):
        """
        Register a new user.

        Args:
            request (Request): The request object.
            *args: Additional positional arguments.
            **kwargs: Additional keyword arguments.

        Returns:
            Response: Response object containing the registered user data or error message.
        """
        data = request.data
        try:
            user = User.objects.create(
                username=data['username'],
                password=make_password(data['password'])
            )
            serializer = UserSerializerWithToken(user, many=False)
            return Response(serializer.data)
        except:
            message = {'detail': 'User with this username or email already exists'}
            return Response(message, status=status.HTTP_400_BAD_REQUEST)


# Delete user:
class DeleteUserView(DestroyModelMixin, GenericAPIView):
    """
    A view to delete a user.

    Attributes:
        permission_classes (list): The list of permission classes applied to the view.
        queryset (QuerySet): The queryset of User instances.
        serializer_class (Serializer): The serializer class for user deletion.
    """
    permission_classes = [IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def delete(self, request, *args, **kwargs):
        """
        Delete a user.

        Args:
            request (Request): The request object.
            *args: Additional positional arguments.
            **kwargs: Additional keyword arguments.

        Returns:
            Response: Response object indicating the status of the user deletion process.
        """
        return self.destroy(request, *args, **kwargs)