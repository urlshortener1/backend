from django.db import models
from django.utils import timezone

# Create your models here.

class BaseModel(models.Model):
    """
    Base Model for all tables. Will add standard additional fields.
    """
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True