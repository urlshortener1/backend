from django.db import transaction
from celery import shared_task
from celery.utils.log import get_task_logger
from .models import ShortcodePool
from .utils import generate_shortcode

MAX_RETRIES = 5
TARGET_POOL_SIZE = 50
logger = get_task_logger(__name__)


@shared_task
def replenish_shortcode_pool(target_pool_size: int = TARGET_POOL_SIZE):
    """
    Asynchronous task to replenish the shortcode pool in the database.

    Args:
        target_pool_size (int, optional): The desired size of the shortcode pool. Defaults to TARGET_POOL_SIZE.

    This task calculates the difference between the current size of the shortcode pool and the desired target size.
    For each missing shortcode needed to reach the target pool size, the task attempts to generate a unique shortcode
    and adds it to the pool. If the maximum number of retries is reached without successfully generating a unique
    shortcode, an error message is logged.

    Note:
        This task is designed to be executed asynchronously using Celery.

    """
    current_pool_size = ShortcodePool.objects.filter(is_used=False).count()

    needed_shortcodes = target_pool_size - current_pool_size
    
    for _ in range(needed_shortcodes):
        unique = False
        retries = 0
        while not unique and retries < MAX_RETRIES:
            shortcode = generate_shortcode()
            with transaction.atomic():
                if not ShortcodePool.objects.filter(shortcode=shortcode).exists():
                    ShortcodePool.objects.create(shortcode=shortcode)
                    unique = True
                retries += 1
        
        if not unique:
            logger.error("Failed to generate a unique shortcode after maximum retries.")
