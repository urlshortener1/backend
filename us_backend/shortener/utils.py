import random
import hashlib
import base64
from datetime import datetime
import uuid
import string
import secrets

from django.conf import settings

SHORTCODE_MIN = getattr(settings, "SHORTCODE_MIN", 8)
SHORTCODE_MAX = getattr(settings, "SHORTCODE_MAX", 15)
MAX_RETRIES = 10

def generate_shortcode(size_min: int = SHORTCODE_MIN, size_max: int = SHORTCODE_MAX) -> str:
    """
    Generate a random shortcode string.

    Args:
        size_min (int): The minimum length of the shortcode (default: settings.SHORTCODE_MIN or 8).
        size_max (int): The maximum length of the shortcode (default: settings.SHORTCODE_MAX or 15).

    Returns:
        str: The generated random shortcode string.

    Notes:
        This function generates a random shortcode string by hashing a combination of a UUID, timestamp, and salt,
        then truncating the hash to the desired length. The nonce (UUID), timestamp, and salt are combined
        to ensure uniqueness and collision avoidance.
    """

    # The idea of a nonce is to be unique, mostly, it's usually an incremental number that will never repeat, but since it would require another logic to 
    # implement, I'm using a uuid as a nonce, since it's gonna be unique.
    nonce = str(uuid.uuid4())
    
    # Here I added the timestamp and a salt as secure ways to avoid collisions
    str_timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    
    salt_length = 16
    salt_chars = string.ascii_letters + string.digits
    salt = ''.join(secrets.choice(salt_chars) for _ in range(salt_length))

    # Here we combine all that together in a single string:    
    input_for_hashing = f"{nonce}{str_timestamp}{salt}"
    
    # Now we create a SHA-256 hash of the URL
    hash_object = hashlib.sha256(input_for_hashing.encode())
    
    # We convert the hash to a base64 string to shorten it and make it URL-friendly 
    # (meaning ensuring that it can be safely used in a URL without causing issues due to reserved characters, encoding problems, or readability issues)
    base64_hash = base64.urlsafe_b64encode(hash_object.digest()).decode('utf-8')
    
    # Now we pick a random length
    length = random.randint(size_min, size_max)
    
    # Here we truncate the hashed str to the desired length
    shortcode = base64_hash[:length]
    
    return shortcode