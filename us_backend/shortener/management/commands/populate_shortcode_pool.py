# management/commands/populate_shortcode_pool.py
from django.core.management.base import BaseCommand
from shortener.models import ShortcodePool
from shortener.utils import generate_shortcode
from django.db import transaction

MAX_RETRIES = 5


class MaxRetriesExceededException(Exception):
    pass


class Command(BaseCommand):
    help = 'Populates the pool of shortcodes'  
    
    def add_arguments(self, parser):
        # Optional argument to force population even if the table isn't empty
        parser.add_argument(
            '--force',
            action='store_true',
            help='Forcefully populate the shortcode pool, even if it is not empty.',
        )
    
    def handle(self, *args, **options):
        
        force_populate = options['force']
        
        if force_populate or not ShortcodePool.objects.exists():
            count_msg = "Forcefully populating..." if force_populate else "The ShortcodePool table is empty. Populating..."
            self.stdout.write(count_msg)
        
            for _ in range(10):  # We could adjust this number depending on demand, though it's just to populate the ShortcodePool table for the first time
                unique = False  
                retries = 0
                while not unique and retries < MAX_RETRIES:
                    shortcode = generate_shortcode()
                    with transaction.atomic():
                        if not ShortcodePool.objects.filter(shortcode=shortcode).exists():
                            ShortcodePool.objects.create(shortcode=shortcode)
                            unique = True
                        retries += 1
                if not unique and retries == MAX_RETRIES:
                    raise MaxRetriesExceededException("Failed to generate a unique shortcode after maximum retries.")
                    
            self.stdout.write(self.style.SUCCESS('Successfully populated the shortcode pool.'))

        else:
            self.stdout.write(self.style.WARNING('The ShortcodePool table is not empty. Use --force to populate anyway.'))