from django.contrib import admin
from .models import ShortURL, ShortcodePool
# Register your models here.

class ShortURLAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'url', 'shortcode', 'timestamp', 'active']
    
class ShortcodePoolAdmin(admin.ModelAdmin):
    list_display = ['id', 'shortcode', 'is_used']
    
    
    

admin.site.register(ShortURL, ShortURLAdmin)
admin.site.register(ShortcodePool, ShortcodePoolAdmin)