from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import ListCreateAPIView
from .models import ShortURL
from .serializers import ShortURLSerializer

# Create your views here.
class ShortURLListCreateAPIView(ListCreateAPIView):
    """
    A view that handles the creation and listing of ShortURL instances.

    Attributes:
        queryset (QuerySet): The queryset of ShortURL instances.
        serializer_class (Serializer): The serializer class for ShortURL instances.
        permission_classes (list): The list of permission classes applied to the view.
    """
    queryset = ShortURL.objects.all()
    serializer_class = ShortURLSerializer
    permission_classes = [IsAuthenticated]
    
    
    def get_queryset(self):
        """
        Retrieve the queryset of ShortURL instances created by the requesting user.

        Returns:
            QuerySet: Filtered queryset of ShortURL instances.
        """
        
        return ShortURL.objects.filter(user=self.request.user)
    
    
    def create(self, request, *args, **kwargs):
        """
        Create a new ShortURL instance based on the provided data.

        Args:
            request (Request): The request object.
            *args: Additional positional arguments.
            **kwargs: Additional keyword arguments.

        Returns:
            Response: Response object containing the created ShortURL data or error message.
        """
        try:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            
            instance = serializer.instance
            
            # Here we grab the absolute url to build the shortened url:        
            base_url = request.build_absolute_uri('/')

            shortened_url = f"{base_url}{instance.shortcode}/"
            
            response_data = {
                "id": instance.id,
                "user": instance.user.id,
                "url": instance.url,
                "short_code": instance.shortcode,
                "timestamp": instance.timestamp,
                "created_at": instance.created_at,
                "updated_at": instance.updated_at,
                "shortened_url": shortened_url
            }
            return Response(response_data, status=status.HTTP_201_CREATED)
        except Exception as e:
            print("ERROR ON POST REQUEST: ", e)
    
# Redirect URL:
def redirect_to_original_url(request, shortcode):
    """
    Redirect the user to the original URL associated with the given shortcode.

    Args:
        request (Request): The request object.
        shortcode (str): The shortcode identifying the original URL.

    Returns:
        HttpResponseRedirect: Redirect response to the original URL.
    """
    short_url = get_object_or_404(ShortURL, shortcode=shortcode)
    original_url = short_url.url
    return redirect(original_url)