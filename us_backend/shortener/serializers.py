from rest_framework import serializers
from .models import *



class ShortURLSerializer(serializers.ModelSerializer):
    shortened_url = serializers.SerializerMethodField()
    
    def get_shortened_url(self, obj):
        request = self.context.get('request')
        if request is not None:
            base_url = request.build_absolute_uri('/')
            return f"{base_url}{obj.shortcode}"
        return None
    
    class Meta:
        model = ShortURL
        fields = ['id', 'user', 'url', 'shortcode', 'timestamp', 'created_at', 'updated_at','shortened_url']
        read_only_fields = ['id', 'shortcode', 'timestamp', 'created_at', 'updated_at','shortened_url']