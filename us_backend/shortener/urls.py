from django.urls import path
from .views import ShortURLListCreateAPIView

urlpatterns = [
    path('shorturls/', ShortURLListCreateAPIView.as_view(), name='shorturls-list-create'),
]
