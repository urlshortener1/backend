from django.db import models, IntegrityError, transaction
from django.conf import settings
from django.contrib.auth import get_user_model

from base.models import BaseModel

# Create your models here.

User = get_user_model()

SHORTCODE_MAX = getattr(settings, "SHORTCODE_MAX", 15)


class ShortcodePool(models.Model):
    shortcode = models.CharField(max_length=15, unique=True)
    is_used = models.BooleanField(default=False)

    def __str__(self):
        return self.shortcode
    

class ShortURL(BaseModel):
    user        = models.ForeignKey(User, on_delete=models.CASCADE)
    url         = models.CharField(max_length=220)
    shortcode   = models.CharField(max_length=SHORTCODE_MAX, unique=True, blank=True)
    timestamp   = models.DateTimeField(auto_now_add=True)
    active      = models.BooleanField(default=True)
    
    
    def save(self, *args, **kwargs):
        # Check if a shortcode has not already been assigned
        if not self.shortcode:  
            with transaction.atomic(): 
                # Try to get an unused shortcode from the pool
                
                # I guess theoretically 2 ShortURLs could possible pick the same shortcode from the ShortcodePool table. That's something that I looked into
                # a bit and can be handled, but due to the lack of time didn't dive deeper to implement a way around that
                shortcode_obj = ShortcodePool.objects.filter(is_used=False).first()
                
                if shortcode_obj:
                    self.shortcode = shortcode_obj.shortcode
                    shortcode_obj.is_used = True
                    shortcode_obj.save()
                else:
                    # We could handle here for the case that we are on high demand and the chances of running out of shortcodes available becomes high
                    raise ValueError("No unused shortcodes are available.")
                    

        try:
            with transaction.atomic():
                super(ShortURL, self).save(*args, **kwargs)
        except IntegrityError:
            raise ValueError("Failed to save ShortURL due to shortcode integrity error.")
    
    def __str__(self):
        return self.url