#!/bin/sh

set -e

cd us_backend

# Apply collectstatic
python manage.py collectstatic --noinput

# Apply database migrations
python manage.py migrate

# Populate the ShortcodePool table
python manage.py populate_shortcode_pool

# Start Celery worker in the background
celery -A us_backend worker --loglevel=info &

# Start Django development server
exec python manage.py runserver 0.0.0.0:8000

# For production:
# exec uwsgi --socket :9000 --workers 4 --master --enable-threads --module 